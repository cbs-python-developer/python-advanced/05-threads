"""Homework 5-2 | Processing an event."""

import logging
import threading
import time
from pathlib import Path


FILE = Path(__file__).parent / 'wow_file.txt'
FRAGMENT = 'Wow!'


def handler(file: Path, fragment: str) -> None:
    """Create a file. Add the text fragment to the file."""
    logging.info('Handler : started')
    time.sleep(8)
    file.touch()
    logging.info('Handler : %s created', file.name)

    time.sleep(8)
    with open(file, 'a') as file_obj:
        file_obj.write(fragment)
        logging.info('Handler : added "%s" to %s', fragment, file.name)


def is_string_in_file(file_path: Path, txt_string: str) -> bool:
    """Whether the string is in the file."""
    with open(file_path, 'r') as file_obj:
        if file_obj.read().find(txt_string) == -1:
            return False
        else:
            return True


def searcher(file: Path, fragment: str) -> None:
    """Look for the text fragment in the file if it exists."""
    logging.info('Searcher: started')
    while True:
        if file.exists():
            logging.info('Searcher: %s found', file.name)
            break
        else:
            time.sleep(5)
    while True:
        if is_string_in_file(file, fragment):
            logging.info('Searcher: "%s" found in %s', fragment, file.name)
            wow_event.set()
            wow_event.clear()
            break
        else:
            time.sleep(5)


def cleaner(file: Path) -> None:
    """Delete the file once the event occurs."""
    logging.info('Cleaner : started')
    wow_event.wait()
    file.unlink()
    logging.info('Cleaner : %s deleted', file.name)


if __name__ == '__main__':

    # logging configuration
    massage_format = "%(asctime)s: %(message)s"
    date_format = "%Y-%m-%d %H:%M:%S"
    logging.basicConfig(
        format=massage_format, level=logging.INFO, datefmt=date_format
    )

    wow_event = threading.Event()

    task1 = threading.Thread(target=handler, args=(FILE, FRAGMENT))
    task2 = threading.Thread(target=searcher, args=(FILE, FRAGMENT))
    task3 = threading.Thread(target=cleaner, args=(FILE, ))

    task1.start()
    task2.start()
    task3.start()

    task1.join()
    task2.join()
    task3.join()
