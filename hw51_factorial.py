"""Homework 5-1 | Factorial functions using threads."""

import concurrent.futures
import functools
import multiprocessing
import threading
import time
from typing import Any, Callable, Iterator


def time_meter(func: Callable) -> Callable:
    """Measure the execution time of a function."""
    @functools.wraps(func)
    def _wrapper(*args, **kwargs) -> Any:
        """Inner function."""
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(
            f"Function '{func.__name__}' "
            f"took {(end_time - start_time) * 1000:.4f} milliseconds."
        )
        return result
    return _wrapper


def factorial(n: int) -> int:
    """Calculate the factorial of N."""
    if n == 0:
        return 1
    else:
        result = 1
        for idx in range(1, n + 1):
            result *= idx
        return result


@time_meter
def find_factorial_synch(*numbers: int) -> list[int]:
    """Find the factorial of numbers."""

    results: list = []
    for number in numbers:
        results.append(factorial(number))

    return results


@time_meter
def find_factorial_thread(*numbers: int) -> list[int]:
    """Find the factorial of numbers using Threads."""
    def factorial_inner(n: int) -> None:
        """Calculate the factorial of N."""
        if n == 0:
            result = 1
        else:
            result = 1
            for idx in range(1, n + 1):
                result *= idx
        results.append(result)

    results: list = []
    tasks: list = []

    for number in numbers:
        task = threading.Thread(target=factorial_inner, args=(number,))
        task.start()
        tasks.append(task)

    for task in tasks:
        task.join()

    return results


@time_meter
def find_factorial_threadpool(*numbers: int) -> Iterator[int]:
    """Find the factorial of numbers using ThreadPoolExecutor."""

    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        results = executor.map(factorial, numbers)
        return results


@time_meter
def find_factorial_multiprocessing(*numbers: int) -> list[int]:
    """Find the factorial of numbers using multiprocessing."""

    with multiprocessing.Pool() as pool:
        results = pool.map(factorial, numbers)
        return results


if __name__ == "__main__":
    functions = (
        find_factorial_synch,
        find_factorial_thread,
        find_factorial_threadpool,
        find_factorial_multiprocessing,
    )
    nums = [1111] * 1000
    for function in functions:
        function(*nums)
